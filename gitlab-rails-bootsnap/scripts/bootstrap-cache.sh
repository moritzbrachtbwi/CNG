#!/usr/bin/env bash

cd /srv/gitlab

# Set up database
sed -i -e "s/host: localhost/host: postgres/" /srv/gitlab/config/database.yml
sed -i -e "s/username: git/username: postgres/" /srv/gitlab/config/database.yml

export ENABLE_BOOTSNAP=1
export BOOTSNAP_CACHE_DIR=/srv/gitlab/bootsnap
export RUBYOPT=-W:no-experimental
export RAILS_ENV=production

bundle exec rake db:setup
bundle exec bin/rails runner "puts 'Done'"
