ARG RUBY_IMAGE=

ARG UID=1000

FROM ${RUBY_IMAGE} as composite

ARG UID

ADD gitlab-rails-ee.tar.gz /assets
ADD gitlab-gomplate.tar.gz /assets
ADD gitlab-exiftool.tar.gz /assets

RUN chown -R ${UID}:0 /assets/srv/gitlab \
    && chmod o-w /assets/srv/gitlab \
    && chmod -R g=u /assets/srv/gitlab

FROM ${RUBY_IMAGE} as final

ARG GITLAB_VERSION
ARG GITLAB_USER=git
ARG UID
ARG GITLAB_DATA=/var/opt/gitlab
ARG DNF_OPTS

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-rails" \
      name="GitLab Rails" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_VERSION} \
      release=${GITLAB_VERSION} \
      summary="Rails container for GitLab." \
      description="Rails container for GitLab."

RUN microdnf ${DNF_OPTS} install --best --assumeyes --nodocs --setopt=install_weak_deps=0 \
            libicu tzdata uuid gzip \
            libpng libjpeg-turbo zlib libtiff shadow-utils \
    && microdnf clean all \
    && adduser -m ${GITLAB_USER} -u ${UID} \
    && mkdir -p ${GITLAB_DATA}/{.upgrade-status,data,repo,config} \
    && chown -R ${UID}:0 ${GITLAB_DATA} \
    && chmod -R ug+rwX,o-rwx ${GITLAB_DATA}/repo \
    && chmod -R ug-s ${GITLAB_DATA}/repo \
    # remove shadow-utils within the same layer
    && microdnf remove shadow-utils \
    && microdnf clean all

COPY scripts/ /scripts
COPY --from=composite /assets/ /

RUN chown -R ${UID}:0 /scripts /home/${GITLAB_USER} \
    && chmod o-w /scripts/lib /scripts/lib/checks \
    && chmod -R g=u /scripts /home/${GITLAB_USER} \
    && mv /srv/gitlab/log/ /var/log/gitlab/ \
    && ln -s /var/log/gitlab /srv/gitlab/log \
    && cd /srv/gitlab \
    && mkdir -p public/uploads \
    && chown -R ${UID}:0 public/uploads \
    && chmod 0700 public/uploads \
    && chmod o-rwx config/database.yml \
    && chmod 0600 config/secrets.yml \
    && chmod -R u+rwX builds/ shared/artifacts/ \
    && chmod -R ug+rwX shared/pages/ \
    && mkdir /home/git/gitlab-shell \
    && chown ${UID}:0 /home/git/gitlab-shell \
    && chmod -R g=u /home/git/gitlab-shell \
    && ln -s /srv/gitlab/GITLAB_SHELL_VERSION /home/git/gitlab-shell/VERSION \
    && sed -e '/host: localhost/d' -e '/port: 80/d' -i config/gitlab.yml \
    && sed -e "s/# user:.*/user: ${GITLAB_USER}/" -e "s:/home/git/repositories:${GITLAB_DATA}/repo:" -i config/gitlab.yml \
    && ldconfig

ENV RAILS_ENV=production \
    BOOTSNAP_CACHE_DIR=/srv/gitlab/bootsnap \
    RUBYOPT="-W:no-experimental" \
    EXECJS_RUNTIME=Disabled \
    CONFIG_TEMPLATE_DIRECTORY=/srv/gitlab/config \
    UPGRADE_STATUS_DIR=${DATADIR}/.upgrade-status

VOLUME ${GITLAB_DATA}


