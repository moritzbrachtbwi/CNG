#!/bin/bash
# This script designed to download and verify the sources of GraphicsMagick
# Items from environment:
# - GM_SOURCE_URL - complete URL of the source tarball
# - GM_SIGNATURE_URL - complete URL of the detatched signature
# - GM_CHECKSUM_SHA256 - sha256 of the source tarball to verify
# - _SIGNATURE_KEY - The key fingerprint to have created the signature
# - _SIGNATURE_ENFORCE - Enable tarball signature verification

set -exo pipefail

pushd /build
# Downloads sources
curl --retry 6 -fLsS -o graphicsmagick.tarball $GM_SOURCE_URL

# Verify checksum
if [ "$GM_CHECKSUM_SHA256" ]; then
  echo "${GM_CHECKSUM_SHA256} ./graphicsmagick.tarball" > SHASUM
  sha256sum -c SHASUM
fi

# Download & verify signature
if [ -z "$_SIGNATURE_ENFORCE" ] && [ -z "$_SIGNATURE_KEY" ] ; then
  curl --retry 6 -fLsS -o graphicsmagick.tarball.sig $GM_SIGNATURE_URL
  gpg --recv-key $_SIGNATURE_KEY
  gpg graphicsmagick.tarball.sig
fi

tar xf graphicsmagick.tarball

popd # from /build